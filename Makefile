.PHONY: test    # run the test suite
.PHONY: lint    # run gawk's linter
.PHONY: doc     # build the docs from the source

BATS      = vendor/bats/bin/bats
BATS_REPO = https://github.com/bats-core/bats-core.git

vendor:
	mkdir -p $@

$(BATS): | vendor
	git clone $(BATS_REPO) vendor/bats

test: | $(BATS)
	$(BATS) tests/*


lint:
	@echo -n | awk --lint=invalid -f awk-tpl.awk \
2>&1 | awk '! /awk-tpl.awk:9.*dotenvfile/' # suppress unwanted warnings


README:
	awk '/^#/, /^#/ { gsub(/^#+ ?/, "", $$0); print }' awk-tpl.awk > $@

doc: README
