##
# awk-tpl.awk - a simple templating program
# =========================================
#
# This is a simple templating program intended to generate config files on UNIX
# systems.
#
# It reads the variables definitions in the `.env` file in the working directory
# and replaces `%var_name%` tags with their respective values. The path to the
# `.env` file can be set with the `dotenvfile` variable.
#
# You can also include template files by inserting a line like :
#
# ```
# %include path/to/template
# ```
##

BEGIN {
    # use a default location for the .env file if not provided as variable
    if (dotenvfile == 0) # suppress warning
        dotenvfile = ".env"

    FS = "[=[:space:]]+"
    while (getline <dotenvfile > 0) {
        if (match($0, "=")) {
            var_name = $1
            # use the shell to parse the .env file for us.
            ". " dotenvfile " && echo $" $1 | getline value
            vars[var_name] = value
        }
    }
    FS = " "
}

/^%include/ {
    filename = $2
    while ("awk -f awk-tpl.awk -v dotenvfile=" dotenvfile " " filename | getline line > 0) {
        print line
    }
}

! /^%include/ {
    # Substitute every %variable% with its value.
    for (var_name in vars)
        gsub("%" var_name "%", vars[var_name], $0)

    # unescape escaped %s
    gsub("\\\\%", "%", $0)

    print
}
