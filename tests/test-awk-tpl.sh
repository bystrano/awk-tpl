#!/usr/bin/env bats

@test "do not harm" {
    result="$(echo 'hello' | awk -f awk-tpl.awk)"
    [ "$result" == 'hello' ]
}

@test "replace values" {
    result="$(echo '->%my_var%<-' | awk -f awk-tpl.awk -v dotenvfile=tests/env)"
    [ "$result" == '->my_value<-' ]
}

@test "unquote quoted %s" {
    result="$(echo '->\%my_var\%<-' | awk -f awk-tpl.awk -v dotenvfile=tests/env)"
    [ "$result" == '->%my_var%<-' ]
}

@test "dotenv with comments and quotes" {
    result="$(echo '->%my_var%<-' | awk -f awk-tpl.awk -v dotenvfile=tests/env-rich)"
    [ "$result" == '->my_value<-' ]
}

@test "dotenv with multiline declarations" {
    result="$(echo '->%my_multiline_var%<-' | awk -f awk-tpl.awk -v dotenvfile=tests/env-rich)"
    [ "$result" == '->this is a multiline string<-' ]
}

@test "simple include" {
    result="$(echo '%include tests/inc.txt' | awk -f awk-tpl.awk -v dotenvfile=tests/env)"
    [ "$result" == 'included text : my_value
second line
%%' ]
}
